# Getting started with MOSIP Pre-Registration User Interface

Eager to try out the mosip pre-registration module? Here are the instructions for setting it up and giving it a spin. Deploying mosip involves building and creating deployable images of the software, setting up dependencies, updating the configuration and creating a kubernetes cluster and running the mosip micro services on it. The following sections will address these in steps.

/mosip-infra/scripts/build-deploy has scripts for setting up the environment, the build box and the actual build scripts. The environment folder has scripts that can be run to setup the dependencies for mosip such as antivirus, ldap, postgres, nginx proxy, hdfs and ssl certificate. The dependencies required can be setup in either a single VM or multiple Virtual Machines. Each of the services described can be hosted separately.


## Setup Dependency Environment

mosip uses postgres to store data, hdfs to store files, and ldap for authorization information. In addition to these the file service requires an external virus scanning service. Since the microservices are deployed on a kubernetes cluster, a proxy service is also needed to forward requests as well as handle https. Let us setup each of these one by one.

The scripts referred to in the sections below are for a RHEL/CentOS 7.6 environment. If you choose alternate environments your setup process might vary. Also the scripts prompt the running of manual commands in places. These instructions are to be followed to complete the setup as intended.


### Setting up postgres database

Refer to the steps in the script psql-setup.sh for setting up your postgres server. It is recommended that you follow the script as closely as possible to keep configuration changes to a minimum.

### Setting up hdfs

Refer to the steps in the script hdfs-setup.sh for getting your hdfs environment up and running.

### Setting up antivirus

Refer to the steps in the script clam-setup.sh to setup clam antivirus on a virtual machine.

### Setting up ldap

Refer to the steps in the script ldap-setup.sh to setup Apache Directory Services as your ldap provider.

### Proxy setup

NGINX is used as the proxy. It has to be setup with SSL support. Use the following scripts to set these up. Follow the steps in proxy-setup.sh and then ssl-setup.sh to get your proxy running.

### Kubernetes setup

We used the Azure kubernetes service and setup a cluster with 5 nodes. The nodes had 4 vcpus and 8 GB RAM each. We recommend that you try the same. You are however free to use any kubernetes environment with your own node configurations.


### Steps to setup and configure Kubernetes Azure

Install azurecli and kubectl applications in local and run the below commands

To get the config file from azure

az aks get-credentials --resource-group  <resource group name> --name <kubernet cluster name>

Create a vm for deployment scripts.

sudo  apt-get install docker docker-compose

sudo  apt-get install maven

sudo apt-get install openjdk-8-jdk

Copy the dockerlist.txt, puhdocker.sh and tagdocker.sh

sftp mosip-open@13.82.17.182

And use put command to copy from the localmachine.

Deploying the SCHEMA for psql.

Docker issue in connecting to the registry following steps was the solution from the link

https://stackoverflow.com/questions/50151833/cannot-login-to-docker-account


Kubernetes secret key generation

Create ssh key on the server as follow 

ssh-keygen -t rsa -b 4096 -C "yourmail@server.com"

Add the public key to the github account 

Then login into the github to get the known_host folder to get created if its not present.

ssh -T git@github.com


Create a secret key 

kubectl create secret generic config-server-secret --from-file=id_rsa=$HOME/.ssh/id_rsa --from-file=id_rsa.pub=$HOME/.ssh id_rsa.pub --from-file=known_hosts=$HOME/.ssh/known_hosts

For Encryption Decryption of properties with configuration server

Create keystore with following command:


keytool -genkeypair -alias mosip-config-server -keyalg RSA -keystore server.keystore -keypass Mosipopen@123 -storepass Mosipopen@123 --dname "CN=Mosip,OU=IT,O=Mosip,L=IITB,S=Karnataka,C=Bangalore"

The JKS keystore uses a proprietary format. It is recommended to migrate to PKCS12 which is an industry standard format, migrate it using following command: 

keytool -importkeystore -srckeystore server.keystore -destkeystore server.keystore -deststoretype pkcs12

Create a yml for keystore as below

apiVersion: v1

kind: Secret

metadata:

name: config-server-keystore-values-secret

type: Opaque

data:

alias: < base-64-encoded-alias-for keystore >

password: <  base-64-store-password >

secret: < base-64-encoded-key-secret >

By using encode url https://www.base64encode.org/

Convert alias name keypass and storepass for security of the keys and run the yaml file

Replace the key and run the file.


Create a docker secret and it can be reused again by converting it to the yaml file

kubectl create secret docker-registry pvt-reg-cred --docker-server=mosipstaging.azurecr.io --docker-username=mosipstaging --docker-password=jg3uClHttkE0m+dpWxoqjMjQcGyNKBKb --docker-email=ramesh@mosip.io

Get the docker-reeg.yml by using the command
kubectl get secret pvt-reg-cred -o yaml --export>> Docker-reg.yaml

cat Docker-reg.yaml

apiVersion: v1

data:

.dockerconfigjson: eyJhdXRocyI6eyJtb3NpcHN0YWdpbmcuYXp1cmVjci5pbyI6eyJ1c2VybmFtZSI6Im1vc2lwc3RhZ2luZyIsInBhc3N3b3JkIjoiamczdUNsSHR0a0UwbStkcFd4b3FqTWpRY0d5TktCS2IiLCJlbWFpbCI6InJhbWVzaEBtb3NpcC5pbyIsImF1dGgiOiJiVzl6YVhCemRHRm5hVzVuT21wbk0zVkRiRWgwZEd0Rk1HMHJaSEJYZUc5eGFrMXFVV05IZVU1TFFrdGkifX19

kind: Secret

metadata:

creationTimestamp: null

name: pvt-reg-cred

selfLink: /api/v1/namespaces/default/secrets/pvt-reg-cred

type: kubernetes.io/dockerconfigjson



### Setting up the Key Manager

The Key Manager is used to save Private and Public Keys used for encryption in mosip. This uses SoftHSM and can be setup in a separate VM. Follow the instructions in the keymanager-setup.sh script.


## Building mosip

The build environment is an Ubuntu 18.04 LTS environment. The following are the pre-requisites for this environment:

* Open JDK 8
* Maven 3.x
* Node 10.14 or above
	(curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
	
	sudo  apt-get install nodejs)
* NPM 6.4.1 or above
* Docker latest stable version
* Python (default)
* Kubectl

For building mosip the following has to be done.

run build_all.py from the mosip-infra repo Path: /scripts/build-deploy/build/build_all.py

The above step will build all but the pre-registration ui.

###  To build pre-regrestration ui run the below commands.

Go to  

~/mosip-open/mosip-staging/mosip-prereg-client

Run the below command from the above path mentioned

pushd pre-registration-ui && npm install && npm run-script build -- --prod --base-href . --output-path=dist && popd


## Configuration the system before running

In order to deploy mosip the configuration has to be first updated to point to the mosip environment. Additionally the docker deployment instructions for the kubernetes setup have to point to the correct cluster and the corresponding docker images.

### Database setup

The postgres database created earlier needs to be configured as follows:

* Creation of Roles

Create the users/roles specified here in the postgres database


<table>
<tr><td>service</td><td>Username</td><td>password</td></tr>
<tr><td>Hdfs</td><td>prereg</td><td>Mosip@dev123</td></tr>
<tr><td>psql</td><td>prereguser</td><td>{cipher}AQA4Mmzw3Ys3/g/rBfmm2qyNWbm4Yb+zDedc1Ri1aMV6daUJAc5PVqyaS4JIYXs/QwG1lD7cYcaNNDN5N11FI7XQsbRtiP2BwVJqBllB0NSuGIsA4/nw6yQWKCdYZBBsSZDyaF80cEfUZLzLv8gyYFd/ato07x+2k4Y96CfTyHj0JiIbgI4EJEIjgHW/nKIfofd6BnEQCkanCplWg0KLsfkf389gw1CbkWiGayY9dCeWxaY1BVtfiE9y1yiIEbd+mB9nlv0AncneYau5oCLWCN133maXl4qV9+u0xg9HQ2NDI87YCGq8TYMv/DgdRgDxMhT/qJlB+cPuV6Llq/xz1VmbVDJ2gTzfwnRfL2tCdbAZkUV/x0WKYQ4pCL5juB+FO24=</td></tr>
<tr><td>psql</td><td>kerneluser</td><td>Mosip@dev123</td></tr>
<tr><td>psql</td><td>audituser</td><td>Mosip@dev123</td></tr>
<tr><td>psql</td><td>masteruser</td><td>Mosip@dev123</td></tr>
<tr><td>psql</td><td>kerneluser</td><td>Mosip@dev123</td></tr>
<tr><td>psql</td><td>iamuser</td><td>Mosip@dev123</td></tr>
</table>


* Creation of Schemas

Create the schema by running the following scripts in the order specified.
* Move to the path of the database folder which you have downloaded from the repository.f
	 	
		cd ~/scripts/database/mosip_prereg
	 
Please note that the order of execution of these scripts is important. The command to run these scripts is as follows.

	1. This command has to be run only once and conatins the common roles creation scripts that are needed to manage the db.(-f is the filename )	
		psql -h <server-ip / dns name> -U postgres -d postgres -f mosip_role_common.sql
				
	2.  The role creation script that will be used by the application to perform DML operations is defined here.
		
		psql -h <server-ip / dns name> -U postgres -d postgres -f mosip_role_<application_user>.sql
	
	3.  This file contains the database creation script of this module

		psql -h <server-ip / dns name> -U postgres -d postgres -f <database_name>_db.sql
		
	4. The needed privilege / grants scripts assigned to database user / role to access database objects are described in this file.
		
		psql -h <server-ip / dns name> -U postgres -d postgres -f <database_name>_grants.sql
	
	* Creation of Seed Master Data
	
	5. Make sure you are in the folder where you have ddl folder in the directory and files in it. This folder contains all the database data definition language (DDL) scripts to create or alter a database object of this module.
		
		psql -h <server-ip / dns name> -U postgres -d postgres -f <database_name>_ddl_deploy.sql
 	
	Import seed data into the system by running the following set of commands.
 
	6. This folder contains the scripts (insert/update/delete scripts) to create seed data / metadata needed to run this module.
		
		psql -h <server-ip / dns name> -U postgres -d postgres -f <database_name>_dml_deploy.sql

Note: Seed data(dml) is not pesent in all the db. Repeat it the follwing steps for all the db.
 ex. for pre-reg db is given below
 
 		cd ~/scripts/database/mosip_prereg
 
 	1. psql -h psql.mosip.io -U postgres -d postgres -f mosip_role_common.sql
	2. psql -h psql.mosip.io -U postgres -d postgres -f mosip_role_prereguser.sql
	3. psql -h psql.mosip.io -U postgres -d postgres -f mosip_prereg_db.sql
	4. psql -h psql.mosip.io -U postgres -d postgres -f mosip_prereg_grants.sql
	5. psql -h psql.mosip.io -U postgres -d postgres -f mosip_prereg_ddl_deploy.sql
	6. psql -h psql.mosip.io -U postgres -d postgres -f mosip_prereg_dml_deploy.sql


### Application configuration setup

mosip has a configuration server which holds the property files for the micro-services. These files contain information relating to access of dependency services and the deployment environment. These configuration entries have to be suitably modified to reflect your environment for the software to work. Given below is a list of configuration changes that need to be done.


<table>
<tr><td>File</td><td>
mosip/config/pre-registration-qa.properties(branch-tag 0.10.6)
</td></tr>
<tr><td>Line 7</td><td>
javax.persistence.jdbc.url=jdbc:postgresql://52.172.15.162:9001/mosip_prereg?useSSL=false
</td></tr>
<tr><td></td><td></td></tr>
<tr><td>File</td><td>
mosip/config/kernel-qa.properties(branch-tag 0.10.3)
</td></tr>
<tr><td>Line 7</td><td>
mosip.kernel.syncdata.public-key-url=http://qa.mosip.io/v1/keymanager/publickey/{applicationId}


</td></tr>
<tr><td>Line 23</td><td>
mosip.kernel.sms.api=http://api.msg91.com/api/v2/sendsms
</td></tr>
<tr><td>Line 24</td><td>
 mosip.kernel.sms.authkey=269113AG0qI54q5c9887de
</td></tr>
<tr><td>Line 32</td><td>
spring.mail.host=smtp.gmail.com
</td></tr>
<tr><td>Line 34</td><td>
spring.mail.username=mosip.emailnotifier@gmail.com
</td></tr>
<tr><td>Line 36</td><td>
spring.mail.password=mindtree@12
</td></tr>
<tr><td>Line 38</td><td>
spring.mail.port=587
</td></tr>
<tr><td>Line 56</td><td>
mosip.kernel.keymanager-service-publickey-url=https://qa.mosip.io/v1/keymanager/publickey/{applicationId}
</td></tr>
<tr><td>Line 57</td><td>
mosip.kernel.keymanager-service-decrypt-url=https://qa.mosip.io/v1/keymanager/decrypt
</td></tr>
<tr><td>Line 58</td><td>
mosip.kernel.keymanager-service-encrypt-url=https://qa.mosip.io/v1/keymanager/encrypt
</td></tr>
<tr><td>Line 69</td><td>
mosip.kernel.keymanager.softhsm.keystore-pass=1234
</td></tr>
<tr><td>Line 71</td><td>
mosip.kernel.keymanager.softhsm.certificate.common-name=www.mosip.io
</td></tr>
<tr><td>Line 73</td><td>
mosip.kernel.keymanager.softhsm.certificate.organizational-unit=MOSIP
</td></tr>
<tr><td>Line 75</td><td>
mosip.kernel.keymanager.softhsm.certificate.organization=IITB
</td></tr>
<tr><td>Line 77</td><td>
mosip.kernel.keymanager.softhsm.certificate.country=IN 
</td></tr>
<tr><td>Line 135</td><td>
ldap.server.host=52.172.11.190
</td></tr>
<tr><td>Line 136</td><td>
ldap.server.port=10389
</td></tr>
<tr><td>Line 139</td><td>
ldap.admin.password=secret
</td></tr>
<tr><td>Line 223</td><td>
admin_database_url=jdbc:postgresql://52.172.54.231:9001/mosip_kernel
</td></tr>
<tr><td>Line 228</td><td>
audit_database_url=jdbc:postgresql://52.172.54.231:9001/mosip_audit
</td></tr>
<tr><td>Line 234</td><td>
masterdata_database_url=jdbc:postgresql://52.172.54.231:9001/mosip_master
</td></tr>
<tr><td>Line 247</td><td>
keymanager_database_url = jdbc:postgresql://52.172.54.231:9001/mosip_kernel
</td></tr>
<tr><td>Line 253</td><td>
otpmanager_database_url = jdbc:postgresql://52.172.54.231:9001/mosip_kernel
</td></tr>
<tr><td>Line 257</td><td>
syncdata_database_url=jdbc:postgresql://52.172.54.231:9001/mosip_master
</td></tr>
<tr><td>Line 262</td><td>
licensekeymanager_database_url=jdbc:postgresql://52.172.54.231:9001/mosip_master
</td></tr>
<tr><td>Line 287</td><td>
spring.datasource.url=jdbc:postgresql://52.172.54.231:9001/mosip_iam
</td></tr>
<tr><td>Line 293</td><td>
ldap_1_DS.datastore.ipaddress=52.172.11.190
</td></tr>
<tr><td>Line 296</td><td>
db_1_DS.datastore.ipaddress=jdbc:postgresql://52.172.54.231:9001/mosip_iam
</td></tr>
<tr><td>Line 303</td><td>
db_2_DS.datastore.ipaddress=jdbc:postgresql://52.172.54.231:9001/mosip_iam
</td></tr>
<tr><td></td><td></td></tr>
<tr><td>File</td><td>
mosip/config/application-qa.properties(branch-tag 0.10.3)
</td></tr>
<tr><td>Line 250</td><td>
mosip.kernel.virus-scanner.host=104.211.200.46
</td></tr>
<tr><td>Line 252</td><td>
mosip.kernel.virus-scanner.port=3310
</td></tr>
<tr><td>Line 258</td><td>
mosip.kernel.fsadapter.hdfs.name-node-url=hdfs://13.71.115.204:51000
</td></tr>
<tr><td>Line 262</td><td>
mosip.kernel.fsadapter.hdfs.authentication-enabled=false
</td></tr>
<tr><td>Line 264</td><td>
mosip.kernel.fsadapter.hdfs.kdc-domain=NODE-MASTER.SOUTHINDIA.CLOUDAPP.AZURE.COM
</td></tr>
<tr><td>Line 273</td><td>
mosip.kernel.fsadapter.ceph.access-key=P9OJLQY2WS4GZLOEF8LH
</td></tr>
<tr><td>Line 274</td><td>
mosip.kernel.fsadapter.ceph.secret-key=jAx8v9XejubN42Twe0ooBakXd1ihM2BvTiOMiC2M
</td></tr>
<tr><td>Line 275</td><td>
mosip.kernel.fsadapter.ceph.endpoint=http://104.211.219.29:7480
</td></tr>
<tr><td></td><td></td></tr>
<tr><td>File</td><td>
mosip/scripts/kubernetes/commons/DeployServiceIngressService.yaml
</td></tr>
<tr><td>
Line 15</td><td>
loadBalancerIP: 104.211.231.5
</td></tr>
</table>


### Kubernetes configuration

The following configuration updates have to be made in the yaml files in the kubernetes folder.

These are the Kubernetes changes in the yml file to be done before deploying the container.

Git url to be changed as per the project at kernel config yml.

<table>
<tr><td>Line 22</td><td>
image: docker-registry.mosip.io:5000/kernel-config-server</td></tr>
<tr><td>Line 24</td><td>
value: git@github.com:mosip/mosip.git</td><tr>
<tr><td>Line 54</td><td>
- name: pvt-reg-cred</td></tr>
</table>

The profile attribute needs to be changed in kernel and prereg yml along with the below mentioned details.

Image name should be changed.

Image pull credentials to be changed.

In pre-registration ui yml proxy server url has to be provided.


### Procedure to run the yml

* Deploy the files which are in common folder first.
* Deploy config pod.
	changes to be done in config-server-deployment-and-service.yaml check for the below file wethere there are modified.

		Line 21:  image: mosipstaging.azurecr.io/kernel-config-server
		Line 24:  value: git@github.com:mosip-staging/mosip-config.git
		Line 54:        - name: pvt-reg-cred
Make sure kuberenets has the following secrets.

		1. pvt-reg-cred (docker configuration to intract with your docker registry)
		2. config-server-keystore-values-secret
		3. config-server-keystore
		4. config-serer-secret
		5. default-token-vvpwj

* Deploy keymanager server as a container in separate vm.
		docker command
	
	docker run -tid --ulimit memlock=-1  -p 8088:8088 -e spring_config_url_env=<configuere server details setup through proxy server/config> -e spring_config_label_env=<branch from where the config has to fetch> -e active_profile_env=<different profiles like dev/qa/stageing etc.,>  -v softhsm:/softhsm --name kernel-keymanager-service <docker registry url/kernel-keymanager-service>
	
	ex. 
	
	docker run -tid --ulimit memlock=-1  -p 8088:8088 -e spring_config_url_env=https://mosip-staging-test.eastus.cloudapp.azure.com/config -e spring_config_label_env=master -e active_profile_env=qa -v softhsm:/softhsm --name kernel-keymanager-service mosipstaging.azurecr.io/kernel-keymanager-service
		
* Deploy kernel component from kernel-deployment folder.
	
	kubectl apply -f ~/scripts/kubernetes/kernel-deployment/

* Deploy pre-registration component from pre-registration-deployment.

	kubectl apply -f ~/scripts/kubernetes/pre-registration-deployment/

Note: deployment of pods has to be done post the psql ddl and dml scripts run from the db. If there are any changes done in psql db then redeploy the yml files again.

Change spec->template->spec->containers->image from mosipstaging.azurecr.io/kernel-auditmanager-service to <Your Docker Registry>/kernel-auditmanager-service 

III. Change spec->template->spec->imagePullSecrets->name from pvt-reg-cred to <Your docker registry credentials secret> 

IV. Change active_profile_env to whichever profile you want to activate and spring_config_label_env to the branch from which you want to pick up the configuration

V. Save the file and Run kubeclt apply -f kernel-auditmanager-service-deployment-and-service.yml 

After above process is completed, you can run kubectl get services command to see the status of all the MOSIP Services.



###  To build pre-regrestration ui first run the below commands.

Path of the pre-registration code 
	cd  /home/mosip-open/mosip-staging/mosip-prereg-client

Install nodejs version 10.14 and above 

Install npm with version 6.4.1

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -

sudo  apt-get install nodejs

Run the below command from the above path mentioned

pushd pre-registration-ui && npm install && npm run-script build -- --prod --base-href . --output-path=dist && popd

Then run the docker build command

sudo docker build -t mosipstaging.azurecr.io/pre-registration-ui  pre-registration-ui

Push the docker to the docker registry 

sudo docker push mosipstaging.azurecr.io/pre-registration-ui
